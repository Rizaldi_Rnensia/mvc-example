﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC_EX.Models;
using System.Data.Entity;

namespace MVC_EX.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            MVC_EXEntities db = new MVC_EXEntities();
            DbSet<Student> student = db.Students;
            return View(student);
        }

        // GET: Student/Details/5
        public ActionResult Details(int studentID)
        {
            MVC_EXEntities db = new MVC_EXEntities();
            Student student = db.Students.Single<Student>(data => data.StudentID == studentID);
            return View(student);
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(Student student)
        {
            try
            {
                // TODO: Add insert logic here
                MVC_EXEntities db = new MVC_EXEntities();
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int studentID)
        {
            MVC_EXEntities db = new MVC_EXEntities();
            Student student = db.Students.Single<Student>(data => data.StudentID == studentID);
            return View(student);
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(int studentID, Student student)
        {
            try
            {
                // TODO: Add update logic here
                MVC_EXEntities db = new MVC_EXEntities();
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int studentID)
        {
            MVC_EXEntities db = new MVC_EXEntities();
            Student student = db.Students.Single<Student>(data => data.StudentID == studentID);
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int studentID, Student student)
        {
            try
            {
                // TODO: Add delete logic here
                MVC_EXEntities db = new MVC_EXEntities();
                db.Entry(student).State = EntityState.Deleted;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
